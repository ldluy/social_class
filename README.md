# Class

Here several different codes for producing social class are stored. All codes are R-codes, but can easily be translated to for example STATA-code.

**eowright.R**: This code works for EMRAPP -- a Swedish dataset on Small business owners in Sweden, collected 2011

**EGP.R**: this code was constructed and works with Undersökningar om levandsförhållanden (ULF) -- a dataset measuring living conditions in Sweden.
