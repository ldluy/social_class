### Oesch class schema
## Data-set: ISSP 2009

## Original spss-codes can be found: http://people.unil.ch/danieloesch/files/2014/05/Oesch_class_schema_ISSP2010-2011.txt

wd$wrkst <- recode(wd$WRKST, '
1="Employed full time";
2="Employed part-time";
3="Employed less than part-time";
4="Helping family member";
5="Unemployed";
6="Student";
7="Retired";
8="Housewife/man";
9="Permanently disabled";
10="Other not in labour force"', as.factor.result=TRUE)

wd$wrkst <- recode(wd$WRKST,'
1:3=1; 4=0; else=0')

wd$wrkst2 <- recode(wd$WRKST,'
1:3="employed"; 4="not employed"; else="not employed"')


## 1= self-employed
wd$wrktype2 <- recode (wd$WRKTYPE, '
4=1; else=0')


## self-employed with employee =1, self-employed without employee =2
wd$selfemp <- recode (wd$NEMPLOY, '
1=0; 2:4000=1; 9995=2; else=0')


wd$emplrel_r <- ifelse(wd$selfemp == 2, 2, wd$wrkst)
wd$emplrel_r <- ifelse(wd$selfemp == 1, 3, wd$emplrel_r)

table(wd$emplrel_r)

wd$emplrel_r2 <- recode(wd$emplrel_r,'
0 = "Not working";
1 = "Employee";
2 = "Self-employed withouth employees";
3 = "Self-employed with employees";
4 = "Workning for the own family"', as.factor.result=TRUE)

wd$emplno_r <- recode(wd$NEMPLOY,'
c(9995)=0;
1:9=1;
10:hi=2;
NA=-9')

table(wd$emplno_r)

wd$selfem_mainjob <- recode(wd$emplno_r, '
0=2;
1=3;
2=4')

wd$selfem_mainjob <- ifelse (wd$emplrel_r == 1 | wd$emplrel_r == 4, 1, wd$selfem_mainjob)
wd$selfem_mainjob <- ifelse(wd$emplrel_r == 0, -99, wd$selfem_mainjob)

table(wd$selfem_mainjob, exclude=NULL)


## isco
                            
wd$isco88 <- wd$ISCO88
table(wd$isco88, exclude=NULL)

wd$isco_mainjob <- wd$isco88

wd$class16 <- -9

## large self-employed
wd$class16 <- ifelse(wd$selfem_mainjob == 4, 1, wd$class16)

## self-employed professionals
wd$class16 <- ifelse((wd$selfem_mainjob==2 | wd$selfem_mainjob == 2) & (wd$isco_mainjob >=2000 & wd$isco_mainjob <= 2229), 2, wd$class16)

wd$class16 <- ifelse((wd$selfem_mainjob==2 | wd$selfem_mainjob==3) & (wd$isco_mainjob >=2300 & wd$isco_mainjob <= 2470), 2, wd$class16)

## small business owners with employees (3)
wd$class16 <- ifelse ((wd$selfem_mainjob==3) & (wd$isco_mainjob >= 1000 & wd$isco_mainjob <= 1999), 3, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==3) & (wd$isco_mainjob >= 3000 & wd$isco_mainjob <= 9333), 3, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==3) & (wd$isco_mainjob==2230), 3, wd$class16)

## small business owners without employees (4)
wd$class16 <- ifelse((wd$selfem_mainjob==2) & (wd$isco_mainjob >= 1000 & wd$isco_mainjob <= 1999), 4, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==2) & (wd$isco_mainjob >= 3000 & wd$isco_mainjob <= 9333), 4, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==2) & (wd$isco_mainjob == 2230), 4, wd$class16)

# /* Allocate independent with employees when they do not report the number of employees
 
#if (selfem_mainjob=5) and (nsup_r =2) class16_r=1.
#if (selfem_mainjob=5) and (nsup_r =1) class16_r=3.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 1200 and isco_mainjob <= 1239) class16_r=1.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 2000 and isco_mainjob <= 2229) class16_r=2.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 2300 and isco_mainjob <= 2470) class16_r=2.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob = 2230) class16_r=4.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 1000 and isco_mainjob <= 1143) class16_r=4.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 1300 and isco_mainjob <= 1319) class16_r=3.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 3000 and isco_mainjob <= 7899) class16_r=4.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob = 7900) class16_r=3.
#if (selfem_mainjob=5) and (nsup_r =0) and (isco_mainjob >= 7901 and isco_mainjob <= 9333) class16_r=4.


## technical experts (5)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2100 & wd$isco_mainjob <= 2213), 5, wd$class16 )
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 7520), 5, wd$class16)

## Technicans (6)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3100 & wd$isco_mainjob <= 3152), 6, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3210 & wd$isco_mainjob <= 3213), 6, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 3434), 6, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 7900), 6, wd$class16)

## Skilled manual workers (7)
wd$class16 <- ifelse((wd$selfem_mainjob == 1) & (wd$isco_mainjob >= 6000 & wd$isco_mainjob <= 7442), 7, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob == 1) & (wd$isco_mainjob >= 7500 & wd$isco_mainjob <= 7510), 7, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob == 1) & (wd$isco_mainjob >= 8310 & wd$isco_mainjob <= 8312), 7, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob == 1) & (wd$isco_mainjob >= 8324 & wd$isco_mainjob <= 8330), 7, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob == 1) & (wd$isco_mainjob >= 8332 & wd$isco_mainjob <= 8340), 7, wd$class16)

## Low skilled maunal (8)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 8000 & wd$isco_mainjob <= 8300), 8, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 8320 & wd$isco_mainjob <= 8321), 8, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 8331), 8, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 8332 & wd$isco_mainjob <= 8340), 8, wd$class16)

## Higher grade managers and administrators (9)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 1000 & wd$isco_mainjob <= 1239), 9, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2400 & wd$isco_mainjob <= 2429), 9, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2441), 9, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2470), 9, wd$class16)

## Lower-grade managers and administrators (10)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 1300 & wd$isco_mainjob <= 1319), 10, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3400 & wd$isco_mainjob <= 3433), 10, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3439 & wd$isco_mainjob <= 3450), 10, wd$class16)

## Skilled clerks (11)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 4000 & wd$isco_mainjob <= 4112), 11, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 4114 & wd$isco_mainjob <= 4210), 11, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 4212 & wd$isco_mainjob <= 4222), 11, wd$class16)

## Unskilled clerks (12)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 4113), 12, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 4211), 12, wd$class16)
wd$class16 <- ifelse((wd$selfem_mainjob==1) & (wd$isco_mainjob == 4223), 12, wd$class16)

## Socio-cultural professionals (13)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2220 &  wd$isco_mainjob <= 2229), 13,wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2300 &  wd$isco_mainjob <= 2320), 13, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2340 &  wd$isco_mainjob <= 2359), 13, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2430 &  wd$isco_mainjob <= 2440), 13, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2442 &  wd$isco_mainjob <= 2443), 13, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2445), 13, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2451), 13, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2460), 13, wd$class16)

## Socio-cultural semi-professionals (14)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2230), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2330 & wd$isco_mainjob <= 2332), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 2444), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2446 & wd$isco_mainjob <= 2450), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 2452 & wd$isco_mainjob <= 2455),14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 3200), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3220 & wd$isco_mainjob <= 3224), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 3226), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3229 & wd$isco_mainjob <= 3340),14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob >= 3460 & wd$isco_mainjob <= 3472), 14, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob==1) & (wd$isco_mainjob == 3480), 14, wd$class16)

## Skilled service (15)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 3225), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 3227 & wd$isco_mainjob <= 3228), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 3473 & wd$isco_mainjob <= 3475), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5000 & wd$isco_mainjob <= 5113), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 5122), 15,  wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5131 & wd$isco_mainjob <= 5132), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5140 & wd$isco_mainjob <= 5141), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 5143), 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5160 & wd$isco_mainjob <= 5220) , 15, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 8323), 15, wd$class16)

## Low-skilled service (16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5120 &  wd$isco_mainjob <= 5121), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5123 &  wd$isco_mainjob <= 5130), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 5133 &  wd$isco_mainjob <= 5139), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 5142), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 5149), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 5152), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 5230), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob == 8322), 16, wd$class16)
wd$class16 <- ifelse ((wd$selfem_mainjob=1) & (wd$isco_mainjob >= 9100 & wd$isco_mainjob <= 9152), 16, wd$class16)

table(wd$class16)

wd$class8 <- recode(wd$class16,'
c(1,2)=1; c(3,4)=2; c(5,6)=3; c(7,8)=4; c(9,10)=5; c(11,12)=6; c(13,14)=7; c(15,16)=8')

wd$class8b <- ifelse (wd$wrkst == 0, 9, wd$class8) # inkludera de icke arbetande som en kategori.



wd$class5 <- recode(wd$class16,'
c(1,2,5,9,13)=1; c(6,10,14)=2; c(3,4)=3; c(7,11,15)=4; c(8,12,16)=5', as.factor.result = TRUE)

table(wd$class5)

wd$class16 <- recode(wd$class16,'
1="Large employers";
2="Self-employed professionals";
3="Small business owners with employees";
4="Small business owners without employees";
5="Technical experts";
6="Technicians";
7="Skilled manual";
8="Low-skilled manual";
9="Higher-grade managers and administrators";
10="Lower-grade managers and administrators";
11="Skilled clerks";
12="Unskilled clerks";
13="Socio-cultural professionals";
14="Socio-cultural semi-professionals";
15="Skilled service";
16="Low-skilled service"', as.factor.result=TRUE)

wd$class8 <- recode(wd$class8,'
1="Self-employed professionals and large employers";
2="Small business owners";
3="Technical (semi-)professionals";
4="Production workers";
5="(Associate) managers";
6="Clerks";
7="Socio-cultural (semi-)professionals";
8="Service workers";
-9=NA', as.factor.result=TRUE)

wd$class8b <- recode(wd$class8b,'
1 = "Self-employed professionals and large employers";
2 = "Small business owners";
3 = "Technical (semi-)professionals";
4 = "Production workers";
5 = "(Associate) managers";
6 = "Clerks";
7 = "Socio-cultural (semi-)professionals";
8 = "Service workers";
9 = "Not working";
-9=NA', as.factor.result=TRUE)


wd$class5 <- recode(wd$class5,' 
1="Higher-grade service class";
2="Lower-grade service class";
3="Small business owners";
4="Skilled Workers";
5="Unskilled Workers";
-9=NA;', as.factor.result=TRUE)

table(wd$class16)
table(wd$class8)
table(wd$class5)

cbind (levels(wd$class8))
wd$class4 <- recode (wd$class8, '
"(Associate) managers" = "Professionals";
"Clerks" = "Service Workers";                   
"Production workers" = "Production Workers";
"Self-employed professionals and large employers" = "Professionals";
"Service workers" = "Service Workers"; 
"Small business owners" = "Small Business Owners";
"Socio-cultural (semi-)professionals" = "Semi-Professionals";
"Technical (semi-)professionals" = "Semi-Professionals"', as.factor.result = TRUE)
